package ml;



import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import zip.ZipFile;

public class EpubBook extends ZipFile {
	String fullPath=null,title=null,creator=null,fullDir=null;
	String fileURL = null;
	Hashtable resources=new Hashtable();
	Vector spine=new Vector();
	Vector ncx=new Vector();
	NavPoint current=null;
	Handler hnd=null;
	
	/**
	 * Lee parametros del libro
	 * @param file URL del ebook
	 * @param buffSize Tamano del buffer para SeekStream
	 * @throws Exception
	 */
	public EpubBook(String file, int buffSize) throws Exception {
		super(file,buffSize);
		fileURL = file;
		getMetaData();
	}
	
	public class Resource {
		String uri=null;
		boolean exists=false;
		boolean main=false;
		public Resource(String uri){
			this.uri=uri;
			if(getZipEntry(fullDir+uri)!=null){
				exists=true;
			}else if(uri.equals("/")){
				exists=true;
				main=true;
			}
		}
		public boolean existe(){
			return exists;
		}
		public byte[] getContent() throws Exception{
			if(main)
				return getMainPage().getBytes();
			return getFile(fullDir+uri);
		}
		public String getType(){
			if(main)
				return "text/html";
			String t=uri.toLowerCase();
			if(t.endsWith(".xhtml"))
				return "application/xhtml+xml";
			else if(t.endsWith(".jpg") || t.endsWith(".jpeg"))
				return "image/jpeg";
			else if(t.endsWith(".html"))
				return "text/html";
			else if(t.endsWith(".png"))
				return "image/png";
			else if(t.endsWith(".gif"))
				return "image/gif";
			return "";
		}
	}
	
	public String getMainPage(){
		String ret=getSpineOfBook();
		ret=replace(ret, "%TITULO%", title);
		return ret;
	}
	
	public String getSpineOfBook(){
		String ret="";
		String head="<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" " +
				"\"http://www.w3.org/TR/html4/loose.dtd\">\n<HTML lang=\"en\"> " +
				"<HEAD><TITLE>%TITULO%</TITLE></HEAD>\n<BODY BGCOLOR=\"white\"><STRONG>TOC</STRONG>" +
				"<BR><TABLE BORDER=\"0\" WIDTH=\"100%\" SUMMARY=\"\">";
		String item="<TR><A HREF=\"%HREF%\">%TEXT%</A><BR></TR>\n";
		String post="</TABLE></BODY></HTML>";
		int cn=1;
		for(int i=0;i<spine.size();i++){
			Item it=(Item)resources.get(spine.elementAt(i));
			if(it!=null){
				String tab=replace(item,"%HREF%",it.href);
				tab=replace(tab,"%TEXT%","Doc#"+cn++);
				ret+=tab;
			}
		}
		return head+ret+post;
	}

	public String getTOCOfBook(){
		String ret="";
		String head="<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" " +
				"\"http://www.w3.org/TR/html4/loose.dtd\"><HTML lang=\"en\"> " +
				"<HEAD><TITLE>%TITULO%</TITLE></HEAD><BODY BGCOLOR=\"white\"><STRONG>TOC</STRONG>" +
				"<BR><TABLE BORDER=\"0\" WIDTH=\"100%\" SUMMARY=\"\">";
		String item="<TR><A HREF=\"%HREF%\">%TEXT%</A><BR></TR>\n";
		String post="</TABLE></BODY></HTML>";
		for(int i=0;i<ncx.size();i++){
			NavPoint np=(NavPoint)ncx.elementAt(i);
			if(np!=null){
				String tab=replace(item,"%HREF%",np.href);
				tab=replace(tab,"%TEXT%",np.text);
				ret+=tab;
			}
		}
		return head+ret+post;
	}
	
	private static final String metainf="META-INF/container.xml";
	private void getMetaData() throws Exception {
		SAXParser parser=SAXParserFactory.newInstance().newSAXParser();
		hnd=new Handler();
		parser.parse(getFileAsStream(metainf), hnd);
		parser.parse(getFileAsStream(fullPath), hnd);
		Vector v=hnd.getResOfType("application/x-dtbncx+xml");
		String ncxFile=null;
		if(v.size()==0){
			Item it=hnd.getResId("ncx");
			if(it!=null)
				ncxFile=it.href;
		}else{
			ncxFile=((Item)(v.elementAt(0))).href;
		}
		String prepend=fullDir+"/";
		if(fullDir.length()==0)
			prepend="";
		if(ncxFile!=null)
			parser.parse(getFileAsStream(prepend+ncxFile), hnd);
	}
	
	public class Handler extends DefaultHandler {
		int selector=-1;
		public void endDocument() throws SAXException {
		}

		public void endElement(String uri, String localName, String name)
				throws SAXException {
			if(name.equals("navPoint")){
				current=null;
			}
		}
		
		public Vector getResOfType(String type){
			Vector ret=new Vector();
			for(Enumeration i=resources.elements();i.hasMoreElements();){
				Item it=(Item)i.nextElement();
				if(it.mediaType.equals(type)){
					ret.addElement(it);
				}
			}
			return ret;
		}
		
		public Item getResId(String id){
			for(Enumeration i=resources.elements();i.hasMoreElements();){
				Item it=(Item)i.nextElement();
				if(it.id.equals(id)){
					return it;
				}
			}
			return null;
		}
		
		public void startDocument() throws SAXException {
		}

		public void startElement(String uri, String localName, String name,
				Attributes attributes) throws SAXException {
			if(name.equals("rootfile")){
				fullPath=attributes.getValue("full-path");
				int idx=fullPath.lastIndexOf('/');
				if(idx<0)
					fullDir="";
				else
					fullDir=fullPath.substring(0,idx);
			} else if(name.equals("title")){
				selector=1;
			} else if(name.equals("creator")){
				selector=2;
			} else if(name.equals("item")){
				resources.put(attributes.getValue("id"), new Item(attributes.getValue("id"),
						attributes.getValue("href"),
						attributes.getValue("media-type")
						));
			} else if(name.equals("itemref")){
				spine.addElement(attributes.getValue("idref"));
			} else if(name.equals("navPoint")){
				current=new NavPoint(Integer.parseInt(attributes.getValue("playOrder")));
				ncx.addElement(current);
			} else if(name.equals("text")){
				selector=3;
			} else if(name.equals("content")){
				current.setHREF(attributes.getValue("src"));
			}
		}

		public void characters(char[] ch, int start, int length)
				throws SAXException {
			switch(selector){
			case 1: title=new String(ch,start,length); break;
			case 2: creator=new String(ch,start,length); break;
			case 3: if(current!=null )current.setText(new String(ch,start,length)); break;
			}
			selector=-1;
		}
		
	}
	
	public class NavPoint {
		int order;
		String text;
		String href;
		public NavPoint(int o){
			order=o;
		}
		public void setText(String t){
			text=t;
		}
		public void setHREF(String h){
			href=h;
		}
	}
	
	public class Item {
		String id,href,mediaType;
		public Item(String i, String hre, String mediaTyp){
			id=i;
			href=hre;
			mediaType=mediaTyp;
		}
	}
	
	/**
	 * File header:
	 * 
	 * central file header signature 4 bytes 0 (0x02014b50) version made by 2
	 * bytes 4 version needed to extract 2 bytes 6 general purpose bit flag 2
	 * bytes 8 compression method 2 bytes 10 last mod file time 2 bytes 12 last
	 * mod file date 2 bytes 14 crc-32 4 bytes 16 compressed size 4 bytes 20
	 * uncompressed size 4 bytes 24 file name length 2 bytes 28 extra field
	 * length 2 bytes 30 file comment length 2 bytes 32 disk number start 2
	 * bytes 34 internal file attributes 2 bytes 36 external file attributes 4
	 * bytes 38 relative offset of local header 4 bytes 42
	 * 
	 * file name (variable size) extra field (variable size) file comment
	 * (variable size)
	 * 
	 * Digital signature:
	 * 
	 * header signature 4 bytes (0x05054b50) size of data 2 bytes signature data
	 * (variable size)
	 * 
	 * @author elinares
	 * 
	 */
	public static String replace(String from, String search, String replace){
		int index=from.indexOf(search);
		while(index>=0){
			from=from.substring(0,index)+replace+from.substring(index+search.length());
			index=from.indexOf(search, index+1);
		}
		return from;
	}
}
