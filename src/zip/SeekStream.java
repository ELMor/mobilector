package zip;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.file.FileConnection;

public class SeekStream {
	private String fileURL;
	private FileConnection fc = null;
	private InputStream is = null;
	private int offOfView ;
	private int offInView ;
	private int viewSize ;
	private byte ssBuffer[] = null;

	/**
	 * Simulacion parcial de lectura aleatoria para J2ME
	 * @param f URL del fichero a leer
	 * @param bufSize Maximo tama�o del buffer de lectura
	 * @throws IOException
	 */
	public SeekStream(String f, int bufSize) throws IOException {
		this.fileURL=f;
		ssBuffer = new byte[bufSize];
		openFC();
	}

	private void openFC() throws IOException {
		fc=(FileConnection)Connector.open(fileURL);
		is=fc.openInputStream();
		viewSize = is.read(ssBuffer);
		offOfView = 0;
		offInView = 0;
	}

	public void next() throws IOException {
		offOfView += viewSize;
		viewSize = is.read(ssBuffer);
		offInView = 0;
	}

	public int read(byte b[], int off, int mainLen) throws IOException {
		int ssBufferRemaining=viewSize - offInView;
		if (offInView + mainLen > viewSize) {
			System.arraycopy(ssBuffer, offInView, b, off, ssBufferRemaining);
			off += ssBufferRemaining;
			int mainRemaining = mainLen - ssBufferRemaining;
			while (mainRemaining > 0) {
				next();
				int chunkSize=mainRemaining;
				if (chunkSize > viewSize)
					chunkSize = viewSize;
				System.arraycopy(ssBuffer, 0, b, off, chunkSize);
				offInView += chunkSize;
				mainRemaining -= chunkSize;
				off += chunkSize;
			}
		} else {
			System.arraycopy(ssBuffer, offInView, b, off, mainLen);
			offInView += mainLen;
		}
		return mainLen;
	}

	public void skip(int bytes) throws IOException{
		seek(offOfView+offInView+bytes);
	}
	
	public void seek(int pos) throws IOException {
		if (offOfView <= pos && offOfView + viewSize > pos) {
			offInView = pos - offOfView;
		} else {
			if (offOfView+viewSize < pos) {
				while (offOfView+viewSize < pos) {
					offOfView += viewSize;
					viewSize = is.read(ssBuffer);
				}
				offInView = pos-offOfView;
			} else {
				fc.close();
				openFC();
				seek(pos);
			}
		}
	}

	public boolean search(byte[] key) throws IOException {
		int ks = 0;
		while (true) {
			for (; offInView < viewSize; offInView++) {
				if (ssBuffer[offInView] == key[ks]) {
					if (++ks == key.length) {
						seek(offOfView + offInView + 1 - key.length);
						return true;
					}
				} else {
					ks = 0;
				}
			}
			try {
				next();
			} catch (IOException ioe) {
				break;
			}
		}
		return false;
	}
	
	public InputStream getInputStream(){
		return new SSInputStream();
	}
	
	class SSInputStream extends InputStream {
		
		public int read() throws IOException {
			if(offInView<viewSize){
				return ssBuffer[offInView++];
			}else{
				if(viewSize<ssBuffer.length){
					return -1; //EOF
				}else{
					next();
					return read();
				}
			}
		}
		
	}
}
