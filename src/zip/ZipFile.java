package zip;

import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;

import ex.NoMoreEntriesException;

public class ZipFile extends SeekStream {
	Hashtable files=new Hashtable();
	public static final byte cfhs[] = { 0x50, 0x4b, 0x01, 0x02 };
	public static final byte sdhs[] = { 0x50, 0x4b, 0x05, 0x05 };

	public ZipFile(String f, int bufSize) throws Exception {
		super(f, bufSize);
		search(cfhs);
		try {
			do {
				ZipEntry entry = new ZipEntry();
				files.put(entry.fileName, entry);
			} while (true);
		} catch (NoMoreEntriesException e) {
			
		} catch (Exception e2) {
			throw new Exception("Failed to read epub");
		}
	}

	public ZipEntry getZipEntry(String fn){
		return (ZipEntry)files.get(fn);
	}
	
	public class ZipEntry {
		int genPurBitFlg;
		int compMethod;
		int crc32;
		int compSize;
		int uncompSize;
		int offset;
		String fileName;
		String extraField;
		String fileComment;

		/**
		 * 
		 * @param fis
		 * @throws Exception
		 */

		public ZipEntry() throws Exception {
			// Leemos la cabecera
			byte[] buf = new byte[46];
			try {
				read(buf, 0, buf.length);
			} catch (Exception ioe) {
				throw new NoMoreEntriesException();
			}
			int magic = getInt(buf, 0, 4);
			if (magic != 0x02014b50)
				throw new Exception("Zip Entry malformed");
			genPurBitFlg = getInt(buf, 8, 2);
			compMethod = getInt(buf, 10, 2);
			crc32 = getInt(buf, 16, 4);
			compSize = getInt(buf, 20, 4);
			uncompSize = getInt(buf, 24, 4);
			offset = getInt(buf, 42, 4);
			int szName = getInt(buf, 28, 2);
			int szExte = getInt(buf, 30, 2);
			int szComm = getInt(buf, 32, 2);
			byte byName[] = new byte[szName];
			byte byExte[] = new byte[szExte];
			byte byComm[] = new byte[szComm];
			read(byName, 0, szName);
			read(byExte, 0, szExte);
			read(byComm, 0, szComm);
			fileName = new String(byName);
			extraField = new String(byExte);
			fileComment = new String(byComm);
			// Nos posicionamos en la siguiente entrada
		}

	}

	public int getInt(byte[] buf, int off, int size) {
		int ret = 0;
		for (int i = size - 1; i > -1; i--) {
			int b = buf[off + i];
			if (b < 0)
				b += 256;
			ret = (256 * ret) + b;
		}
		return ret;
	}

	public InputStream getFileAsStream(String file) throws Exception {
		byte ret[]=getFile(file);
		if(ret==null)
			return null;
		return new ByteInputStream(ret);
	}
	
	public byte[] getFile(String file) throws IOException, DataFormatException{
		ZipEntry epb=(ZipEntry)files.get(file);
		if(epb==null)
			return null;
		seek(epb.offset);
		byte buf[]=new byte[30];
		read(buf,0,30);
		int sz=getInt(buf, 26, 2);
		sz+=getInt(buf, 28, 2);
		skip(sz);
		byte ret[]=null;
		switch(epb.compMethod){
		case 0:
			ret=new byte[epb.compSize];
			read(ret, 0, ret.length);
			break;
		case 8:
			byte input[]=new byte[epb.compSize];
			read(input, 0, input.length);
			ret=new byte[epb.uncompSize];
			Inflater inflater=new Inflater(true);
			inflater.setInput(input);
			inflater.inflate(ret);
			break;
		default:
			throw new IOException("Compression method not supported "+epb.compMethod);
		}
		return ret;
	}
	


}
