package zip;

import java.io.IOException;
import java.io.InputStream;

public class ByteInputStream extends InputStream {
	byte fuente[]=null;
	int pos=0;
	
	public int read(byte[] b, int off, int len) throws IOException {
		int remaining=fuente.length-pos;
		if(remaining>=len){
			System.arraycopy(fuente, pos, b, off, len);
			pos+=len;
			return len;
		}else{
			System.arraycopy(fuente, pos, b, off, remaining);
			pos+=remaining;
			return remaining==0?-1:remaining;
		}
	}

	public int read(byte[] b) throws IOException {
		return read(b,0,b.length);
	}

	public ByteInputStream(byte src[]){
		fuente=src;
	}
	
	public int read() throws IOException {
		if(fuente==null || pos>=fuente.length)
			return -1;
		return fuente[pos++];
	}

}
